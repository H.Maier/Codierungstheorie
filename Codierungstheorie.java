/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codierungstheorie;

import gurobi.*;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Henning
 */
public class Codierungstheorie {

	
	static GRBEnv env;
	static GRBModel model;
    /**
     * @param args the command line arguments
     */
    static ArrayList<int[][]> MatrizenContainer = new ArrayList<int[][]>();
    static ArrayList<int[][]> GeneratorMatrizenContainer = new ArrayList<int[][]>();

    public static void main(String[] args) {
    	try {
	    		
	    	int k;
	    	int q;
	    	int n;
	    	int b;
	    	
	    	String input ="";
	        
	        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	        
	        System.out.println("Auswahl der Aufgabe:");
	        System.out.println("(1) Matrix A fuer gegebenes k,q (nur Prim)");
	        System.out.println("(2) Gurobi");
	        System.out.println("(3) Aufstellen der Codetabellen:");
	        System.out.println("(4) Matrix AS fuer gegebenes k,q (nur Prim) und S(e)");
	        System.out.println("(5) Matrix AS fuer gegebenes k,q (nur Prim) n und b");
	        System.out.println("");
	        System.out.print("Bitte waehlen Sie eine Aufgabe 1-5:");
	        
			input = in.readLine();
			
	        switch (input) {
		        case "1":{
		                System.out.print("k = ");
		                k = Integer.parseInt(in.readLine());
		                do {
		                System.out.print("q = ");
		                q = Integer.parseInt(in.readLine());
		                if (!isPrime(q)) {
		                System.out.println("Variable q must be a prime nummber!");
		                }
		                } while (!isPrime(q));
		        	
		                printMatrix(createMatrixA(q, k));
		                
		                break;
		        }
		        case "2":{
		        	env = new GRBEnv("CT.log");
	    			model = new GRBModel(env);
	    			
	    			int[] x;
	    			
		                System.out.print("k = ");
		                k = Integer.parseInt(in.readLine());
		                do {
		                System.out.print("q = ");
		                q = Integer.parseInt(in.readLine());
		                if (!isPrime(q)) {
		                System.out.println("Variable q must be a prime nummber!");
		                }
		                } while (!isPrime(q));
		                System.out.print("b = ");
		                b = Integer.parseInt(in.readLine());
		        	
		                createMatrixA(q, k);
		                
		                x= getVektorX(OptimizeProblem(createMatrixA(q, k), b));
		                printVektorX(x);
		                
		                break;
		        }
		        case "3":{
		        	//3.1
		        	ArrayList<GRBVar> xn;
		        	env = new GRBEnv("CT.log");
	    			model = new GRBModel(env);
	    			
	    			model.set(GRB.IntParam.OutputFlag, 0);
	    			
		        	 for (int i = 2; i < 7; i++) {
		                 if (isPrime(i)) {
		                     for (int h = 2; (Math.pow(i, h) - 1) / (i - 1) <= 1000; h++) {
		                    	 for(int b1 = 1; b1 <= 10 ;b1++)
		                    	 {
		                    		 System.out.println("*** b="+b1+" | q="+i+" | k="+h+" *** ");
		                    		 xn = OptimizeProblem(createMatrixA(i, h),b1);
		                    		 printVektorX(getVektorX(xn));
		                    		 System.out.println("");
		                    		 
		                    		 //GeneratorMatrix
		                    		 System.out.println("");
		                    		 System.out.println("GeneratorMatrix:");
		                    		 printMatrix(TransponiereMatrix(createGeneratormatrix(i, h, xn)));
		                    		 //GeneratorMatrizenContainer.remove(0);
		                    		 System.out.println("");
		                    		 
		                    		 //Hamming-Gewicht
		                    		 System.out.println("Hamming-Gewicht: "+ MinGewicht(i, h, b1));
		                    		 System.out.println("");
		                    	 }
		                         
		                     }
		                 }
		             }
		        
		        }
		        case "4":{
		        	//Aufgabe 4
		        	
		        	
		        	System.out.print("k = ");
	                k = Integer.parseInt(in.readLine());
	                do {
	                System.out.print("q = ");
	                q = Integer.parseInt(in.readLine());
	                if (!isPrime(q)) {
	                System.out.println("Variable q must be a prime nummber!");
	                }
	                } while (!isPrime(q));
		        	
		        	env = new GRBEnv("CT.log");
	    			model = new GRBModel(env);
	    			
	    			model.set(GRB.IntParam.OutputFlag, 0);
	    	    	
	    	    	int[][] AS; 
	    	    	int[] c;
	    	    	int[] x;
	    	    	b=3;
	    	    	
	    	    	
		        	
		    		int[][] e = new int[3][3];
		        	
		        	e[0][0]=1; e[0][1]=0; e[0][2]=1;
		        	e[1][0]=0; e[1][1]=1; e[1][2]=0;
		        	e[2][0]=0; e[2][1]=0; e[2][2]=1;
		        	
		        	ArrayList<int[][]> S = new ArrayList<int[][]>();
		        	S.add(e);
		        	
		        	
		        	//x = new int[]{0,1,0,1,1,0,0};
		        	
		        	AS = createMatrixAS(q, k, S);
		        	
		        	x = getVektorX(OptimizeProblem(AS, b)); //Neu
		        	
		        	c = getVektorC(AS);
	    			
	    			int skalar = skalarproduct(c, x);
	    			int d = skalar-b;
	    			for(int i=0; i < c.length;i++) {
	    				System.out.print(c[i]+" | ");
	    			}
	    			
	    			System.out.println("Skalarprodukt <c,x> :"+skalar);
	    			System.out.println("VektorX: ");
	    			printVektorX(x);
	    			System.out.println("");
	    			System.out.println("C["+skalar+","+k+","+d+"]");		
	    			printResult(q,k,S.get(0),AS);
	    			System.out.println("");
	    			
		        	break;
		        	
		        }
		        case "5":{
		        	 	System.out.print("k = ");
		                k = Integer.parseInt(in.readLine());
		                do {
		                System.out.print("q = ");
		                q = Integer.parseInt(in.readLine());
		                if (!isPrime(q)) {
		                System.out.println("Variable q must be a prime nummber!");
		                }
		                } while (!isPrime(q));
		                System.out.print("n = ");
		                n = Integer.parseInt(in.readLine());
		                System.out.print("b = ");
		                b = Integer.parseInt(in.readLine());
		                
		                
		    			env = new GRBEnv("CT.log");
		    			model = new GRBModel(env);
		    			
		    			model.set(GRB.IntParam.OutputFlag, 0);
		    	    	
		    	    	int[][] AS; 
		    	    	int[] c;
		    	    	int[] x;
		    	    	
		    	    	
		    	    	while (true) {
		    	    		
		    	    		ArrayList<int[][]> S = new ArrayList<int[][]>();
		    	    		
		    	    		S.add(getRandomErzeuger(k, q));
		    	    		
		    	    		AS = createMatrixAS(q, k, S);
		    	    		
		    	    		
		    	    		if(AS!=null) {	
		    	    			c = getVektorC(AS);
		    	    			
		    	    			x= getVektorX(OptimizeProblem(AS, b, c));
		    	    			int skalar = skalarproduct(c, x);
		    	    			int d = skalar-b;
		    	    			for(int i=0; i < c.length;i++) {
		    	    				System.out.print(c[i]+" | ");
		    	    			}
		    	    			
		    	    			System.out.println("Skalarprodukt <c,x> :"+skalar);
		    	    			System.out.println("C["+skalar+","+k+","+d+"]");
		    	    					
		    	    			if(skalar>=n) {
		    	    				printResult(q,k,S.get(0),AS);
		    	    				System.in.read();
		    	    			}
		    	    			System.out.println("");
		    	    			
		    	    			try {
		    						Thread.sleep(500);
		    					} catch (InterruptedException ex) {
		    						// TODO Auto-generated catch block
		    						ex.printStackTrace();
		    					}
		    					
		    	    		}
		    	    	}
		    	    	
	        	}
	        }
	
    	} catch (GRBException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    } 

    public static void printVektorX(int[] x) {
    	System.out.println("Vektor X:");
        for(int i=0; i <x.length;i++) {
        System.out.print(x[i]+"|");
        }
    }
    
    public static void printResult(int q, int k, int[][]EM, int[][] AS) {
    	
    	System.out.println("**** Ergebnis ****");
    	System.out.println("q: "+q);
    	System.out.println("k: "+k);
    	System.out.println("ErzeugerMatrix:");
    	printMatrix(EM);
    	System.out.println("Matrix AS:");
    	printMatrix(AS);
    	System.out.println("******************");
    	
    }

    public static int[][] createMatrixA(int q, int k) {
        
        //Matrix deklarieren
        int x = (int) (Math.pow(q, k) - 1) / (q - 1);
        int[][] A;
        A = new int[x][x];

        //Endlicher K�rper bestimmen
        int[][] Fadd = new int[q][q];
        for (int i = 0; i < q; i++) {
            for (int j = 0; j < q; j++) {
                Fadd[i][j] = (i + j) % q;
            }
        }

        int[][] Fmul = new int[q][q];
        for (int i = 0; i < q; i++) {
            for (int j = 0; j < q; j++) {
                Fmul[i][j] = (i * j) % q;
            }
        }

        //Klassenbestimmen
        int[] z = new int[k];
        for (int i = 0; i < k; i++) {
            z[i] = 0;
        }
        z[k - 1] += 1;
        String t = "";

        //Generate all possible codes into V
        int[] V = new int[(int) (Math.pow(q, k) - 1)];
        String[] S = new String[(int) (Math.pow(q, k) - 1)];
        for (int i = 0; i < (int) (Math.pow(q, k) - 1); i++) {
            //Generate code as string
            for (int j = 0; j < k; j++) {
                t += z[j];
            }

            //add +1 to code
            z[k - 1] += 1;
            for (int j = k - 1; j > 0 && z[j] == q; j--) {
                z[j] = 0;
                z[j - 1] += 1;
            }

            //convert codestring to int and reset codestring
            V[i] = Integer.parseInt(t);
            S[i] = t;
            t = "";
        }

        /*
         for (int i = 0; i < (int) (Math.pow(q, k) - 1); i++) {
         System.out.println(S[i]);
         }
         */
        //Generate Klassenarray
        String[] r = new String[x];

        ArrayList<int[]> repraesentanten = printRepraesentanten(q, k);

        for (int i = 0; i < repraesentanten.size(); i++) {

            r[i] = "";

            for (int j = 0; j < repraesentanten.get(i).length; j++) {
                r[i] += Integer.toString(repraesentanten.get(i)[j]);
            }
        }

        /*
         for(int i = 0; i < r.length; i++) {
         System.out.println(r[i]);
         }
         */
        for (int i = 0; i < k; i++) {

        }

        //Matrix berechnen
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < x; j++) {
                int s = 0;
                for (int e = 0; e < k; e++) {
                    //System.out.println("r substring = " + r[i].substring(e, e+1));
                    //System.out.println("r  = " + Integer.parseInt(r[i].substring(e, e+1)));
                    s = Fadd[s][Fmul[Integer.parseInt(r[i].substring(e, e + 1))][Integer.parseInt(r[j].substring(e, e + 1))]];
                }
                if (s == 0) {
                    A[i][j] = 1;
                } else {
                    A[i][j] = 0;
                }

            }
        }

        /*
        //Matrix Ausgeben
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < x; j++) {
                System.out.print(A[i][j] + " ");

            }
            System.out.println();;
        }
        */

        //Matrixablegen
        StoreMatrix(A);
        
        return A;
        /**
         * } catch (IOException ex) {
         * Logger.getLogger(Codierungstheorie.class.getName()).log(Level.SEVERE,
         * null, ex); }*
         */

    }

    public static boolean isPrime(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isMultiple(String n, String m) {
        double x = 0;
        for (int i = 0; i < n.length() && n.charAt(i) != '0' && m.charAt(i) != '0'; i++) {
            if (n.charAt(i) != '0' && m.charAt(i) != n.charAt(i)) {
                return false;
            }
            if (m.charAt(i) != '0' && m.charAt(i) != n.charAt(i)) {
                return false;
            }
            if (x == 0) {
                x = (double) m.charAt(i) / (double) n.charAt(i);
            } else {
                double y = (double) m.charAt(i) / (double) n.charAt(i);
                if (x != y) {
                    return false;
                }
            }
        }

        return true;
    }

    public static int skalarproduct(int[] v1, int[] v2) {
    	
    	int erg = 0;
    	
    	for(int i=0; i < v1.length; i++) {
    		erg += v1[i]*v2[i];
    	}

        return erg;
    }

    public static int[][] getKoerper(int q, int k) {

        int erg[][] = new int[(int) Math.pow(q, k)][k];

        for (int j = 0; j < k; j++) {
            for (int h = 0; h < erg.length; h++) {
                erg[h][j] = h / (int) (Math.pow(q, j)) % q;
            }

        }
        return erg;
    }

    public static int[][] fillBefore(int[][] body, int sizeK) {

        int erg[][] = new int[body.length][sizeK];

        int differenz = sizeK - body[0].length;

        for (int h = 0; h < erg.length; h++) {
            for (int i = 0; i < sizeK; i++) {
                if (i < (differenz - 1)) {
                    erg[h][i] = 0;
                } else if (i == (differenz - 1)) {
                    erg[h][i] = 1;
                } else {
                    erg[h][i] = body[h][((sizeK - 1) - i)];
                }
            }
        }

        return erg;
    }

    public static ArrayList<int[]> printRepraesentanten(int q, int k) {

        ArrayList<int[]> alAll = new ArrayList<int[]>();

        for (int i = k - 1; i >= 0; i--) {
            alAll.addAll(Arrays.asList(fillBefore(getKoerper(q, i), k)));
        }

        for (int i = 0; i < alAll.size(); i++) {
            for (int j = 0; j < alAll.get(i).length; j++) {
               // System.out.print(alAll.get(i)[j]);
            }
          //  System.out.println("");
        }

        return alAll;
    }

    public static void StoreMatrix(int[][] Matrix) {
        MatrizenContainer.add(Matrix);
    }
    
    public static void StoreGeneratorMatrix(int[][] Matrix) {
        GeneratorMatrizenContainer.add(Matrix);
    }

    public static int HammingGewicht(int[] v, int[][] G) {
        int w = 0;
        
        int[] vG = MatrizenVektorMult(v, G);
        
        for (int i = 0; i < vG.length; i++) {
            if (vG[i] != 0 ) {
                w++;
            }
        }

        return w;
    }
    
    public static int MinGewicht(int q, int k, int b) {
    	
    	int min=0;
    	
    	int[][] A = createMatrixA(q, k);
    	
    	ArrayList<int[]> rep = printRepraesentanten(q, k);
    	
    	ArrayList<GRBVar> xn = OptimizeProblem(A, b);
    	
    	int[][] G = createGeneratormatrix(q, k, xn);
    	
    	for(int i=0; i< rep.size(); i++) {
    		if(min==0) {
    			min = HammingGewicht(rep.get(i), G);
    		}else if((min > HammingGewicht(rep.get(i), G)) && (HammingGewicht(rep.get(i), G) !=0)){
    			min = HammingGewicht(rep.get(i), G);
    		}
    	}
    	
    	return min;
    }
    
    public static int Minimaldistanz (int q, int k) {
    	
    	
    	
    	int[][] Fkq = getKoerper(q,k);
    	
    	int[] gewichte = new int[(int)Math.pow(Fkq.length, Fkq.length)-(Fkq.length-1)];
    	
    	int pos=0;
    	
    	for(int i=0; i < Fkq.length; i++) {
    		
    		for(int j=0; j < Fkq.length; j++) {
        		for(int h=0; h < Fkq[0].length; h++) {
        			if(Fkq[i]!=Fkq[j]) {
        				gewichte[pos]++;
        			}
        			pos++;
        		}
        	}
    		
    	}
    	
    	int erg=0;
    	for(int x=0; x < gewichte.length; x++) {
    		if(x==0) {
    			erg=gewichte[0];
    		}else if(erg > gewichte[x]) {
    			erg=gewichte[x];
    		}
    	}
    	
    	return erg;
    }
    
    public static int getHammingAbstand(int[] v1, int[] v2) {
    		
    	int gewicht=0;
    		
		for(int i=0; i < v1.length; i++) {
			if(v1[i]!=v2[i]) {
				gewicht++;
			}
		}
		return gewicht;
    }
    
    public static int[] MatrizenVektorMult(int[] v, int[][] M) {

        int[] erg = new int[M.length];

        for (int i = 0; i < M.length; i++) {
            erg[i] = 0;
            for (int j = 0; j < M[i].length; j++) {
                erg[i] += M[i][j] * v[j];

            }
        }

        return erg;

    }
    
    public static boolean isNullVektor(int[] v) {
    	
    	for(int i=0; i < v.length; i++) {
    		if(v[i]!=0) {
    			return false;
    		}
    	}
    	return true;
    }
    
    public static int[] MatrizenVektorMultInFq(int[] v, int[][] M, int q) {

        int[] erg = new int[M.length];

        for (int i = 0; i < M.length; i++) {
            erg[i] = 0;
            for (int j = 0; j < M[i].length; j++) {
                erg[i] += M[i][j] * v[j];

            }
            erg[i] = erg[i]%q;
        }

        return erg;

    }

    public static ArrayList<GRBVar> OptimizeProblem(int[][] A, int b, int[] c) {
        try {
           
			//create x-Vektor

            ArrayList<GRBVar> xn = new ArrayList<GRBVar>();

            for (int i = 0; i < A[0].length; i++) {
            	xn.add(model.addVar(0, GRB.INFINITY, 0, GRB.INTEGER, "x" + i));
            }

			//Objective to Maximize c*x0 + c*x1 + ... + c*xn-1
            //double c1 = 1.0;

            //Maximize <c,x>
            GRBLinExpr expr = new GRBLinExpr();
            for (int i = 0; i < xn.size(); i++) {
                expr.addTerm(c[i], xn.get(i));
            }
            model.setObjective(expr, GRB.MAXIMIZE);

            //Constrains A[i][j]*xj <= b
           
            for (int i = 0; i < A.length; i++) {
            	expr = new GRBLinExpr();
                for (int j = 0; j < A[i].length; j++) {
                    if (A[i][j] != 0) {
                        expr.addTerm(A[i][j], xn.get(j));
                    }
                }
                model.addConstr(expr, GRB.LESS_EQUAL, b, "c" + i);
            }

            model.optimize();
            /*
            //Output
            for (int i = 0; i < xn.size(); i++) {

                System.out.println(xn.get(i).get(GRB.StringAttr.VarName)
                        + " " + xn.get(i).get(GRB.DoubleAttr.X));

            }
			*/
            //System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));

            // Dispose of model and environment
            //model.dispose();
            //env.dispose();

            return xn;

        } catch (GRBException e) {

            System.out.println("Error code: " + e.getErrorCode() + ". "
                    + e.getMessage());

            return null;
        }

    }
    
    public static ArrayList<GRBVar> OptimizeProblem(int[][] A, int b) {
        try {
           
			//create x-Vektor

            ArrayList<GRBVar> xn = new ArrayList<GRBVar>();

            for (int i = 0; i < A[0].length; i++) {
            	xn.add(model.addVar(0, GRB.INFINITY, 0, GRB.INTEGER, "x" + i));
            }

			//Objective to Maximize c*x0 + c*x1 + ... + c*xn-1
            double c = 1.0;

            //Maximize <c,x>
            GRBLinExpr expr = new GRBLinExpr();
            for (int i = 0; i < xn.size(); i++) {
                expr.addTerm(c, xn.get(i));
            }
            model.setObjective(expr, GRB.MAXIMIZE);

            //Constrains A[i][j]*xj <= b
           
            for (int i = 0; i < A.length; i++) {
            	expr = new GRBLinExpr();
                for (int j = 0; j < A[i].length; j++) {
                    if (A[i][j] != 0) {
                        expr.addTerm(A[i][j], xn.get(j));
                    }
                }
                model.addConstr(expr, GRB.LESS_EQUAL, b, "c" + i);
            }

            model.optimize();
            /*
            //Output
            for (int i = 0; i < xn.size(); i++) {

                System.out.println(xn.get(i).get(GRB.StringAttr.VarName)
                        + " " + xn.get(i).get(GRB.DoubleAttr.X));

            }
			*/
            //System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));

            // Dispose of model and environment
            //model.dispose();
            //env.dispose();

            return xn;

        } catch (GRBException e) {

            System.out.println("Error code: " + e.getErrorCode() + ". "
                    + e.getMessage());

            return null;
        }

    }

    public static int[][] createGeneratormatrix(int q, int k, ArrayList<GRBVar> xn) {
        try {
            int[][] G;
            ArrayList<int[]> r = printRepraesentanten(q, k);
            int l = 0;
            for (int i = 0; i < xn.size(); i++) {
                l += xn.get(i).get(GRB.DoubleAttr.X);
            }
            G = new int[l][];

            int g = 0;
            for (int i = 0; i < xn.size(); i++) {
                for (int j = 0; j < xn.get(i).get(GRB.DoubleAttr.X); j++) {
                    G[g] = r.get(i);
                    g++;
                }
            }

            StoreGeneratorMatrix(G);
            return G;
        } catch (GRBException ex) {
            Logger.getLogger(Codierungstheorie.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static int[][] createMatrixAS(int q, int k, ArrayList<int[][]> S){
    	int[][] AS;
    	
    	ArrayList<ArrayList<int[]>> ZHKs = CreateZHKs(q, k, S);
    	
    	if(ZHKs == null)return null;
    	
    	ArrayList<int[][]> ST = new ArrayList<int[][]>();
    	
    	for(int i=0; i < S.size();i++) {
    		ST.add(TransponiereMatrix(S.get(i)));
    	}
    	
    	ArrayList<ArrayList<int[]>> ZHKsT = CreateZHKs(q,k,ST);
    	if(ZHKsT == null)return null;
    	
    	AS = new int[ZHKsT.size()][ZHKs.size()];
    	
    	for(int i=0; i <ZHKsT.size(); i++) {
    		for(int j=0; j < ZHKs.size(); j++) {
    			
    			for(int sj=0; sj < ZHKs.get(j).size(); sj++) {
    				int skalar = (skalarproduct(ZHKsT.get(i).get(0),ZHKs.get(j).get(sj))%q);
    				if(skalar==0) {
    					AS[i][j]++;
    				}	
    			}
    		}
    	}
    	return AS;
    }
    
    public static ArrayList<ArrayList<int[]>> CreateZHKs (int q, int k, ArrayList<int[][]> S){
    	
    	LinkedList<int[]> Q = new LinkedList<int[]>();
    	ArrayList<ArrayList<int[]>> ZHKs = new ArrayList<ArrayList<int[]>>();
    	 
    	//Get Repraesentaten
    	ArrayList<int[]> repraesentanten = printRepraesentanten(q, k);
    	
    	int[] Nullvektor = new int [k];
    	
    	for(int i=0; i< k; i++) {
    		Nullvektor[i] = 0;
    	}
    	
    	//Create ZHKs   	
    	while(!repraesentanten.isEmpty()) {	
    		int[] r = repraesentanten.remove(0);
    		
    		Q.add(r);
    		
    		ArrayList<int[]> ZHK = new ArrayList<int[]>();
    		
    		while(!Q.isEmpty()) {
    			
    			int[] ri = Q.removeFirst();
    					
    			if(!isRepraesentantInZHKs(ri, ZHKs)) {
    				ZHK.add(ri);
    				
    				for(int i=0; i < S.size(); i++) {
    					
    					int[] rie = MatrizenVektorMultInFq(ri, S.get(i),q);
    					
    					if(isSameVektor(rie, Nullvektor)) {
    						return null;
    					}
    					
    					
    					
    					//201???? MOD nicht vergessen.
    					
    					ArrayList<int[]> repraesentanten2 = printRepraesentanten(q, k);
    					
    					for(int h=0; h < repraesentanten2.size();h++) {
    						
    						int[] l = repraesentanten2.get(h);
    					
	    					if(isLinAbh(rie, l, q)) {
	    						rie = l;
	    						break;
	    					}
    					}
    					
    					if(!isInZHK(rie,ZHK)) {
    						repraesentanten.remove(rie);
    						Q.addLast(rie);
    					}
    				}
    				
    			}
  	
    		}
    		if(!ZHK.isEmpty())
    		ZHKs.add(ZHK);
    	}
    	
    	//printZHKs(ZHKs);
    	
    	return ZHKs;
    }
    
    public static boolean isInZHK (int[] rep, ArrayList<int[]> ZHK) {
    	
    	for(int i=0; i < ZHK.size(); i++) {
    		if(isSameVektor(rep, ZHK.get(i))) {
    			return true;
    		}
    	}
    	
    	return false;
    	
    }
    
    public static boolean isSameVektor(int[] v1, int[]v2) {
    	
    	
    	if(v1.length==v2.length) {
    		for(int i=0; i < v1.length; i++) {
    			if(!(v1[i]==v2[i])) {
    				return false;
    			}
    		}
    		return true;
    	}else {
    		return false;
    	}
    	
    }
     
    public static boolean isRepraesentantInZHKs(int[] rep, ArrayList<ArrayList<int[]>> ZHKs) {
    	for(int i=0; i <ZHKs.size(); i++) {
    		if(isInZHK(rep, ZHKs.get(i))) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public static void printMatrix(int[][] A) {
        //Matrix Ausgeben
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length; j++) {
                System.out.print(A[i][j] + " ");
            }
            System.out.println();;
        }
    }

    public static boolean isLinAbh(int[] r, int[] l, int q){
        //boolean linAbh = false;
        
        
        int[] r2 = new int[r.length];
        /*
        for(int i=0; i < r.length ; i++) {
        	r2[i]=r[i];
        }*/
        
        if(isSameVektor(r2, l)) return true;
        for(int i=1;i<q;i++){
            for(int j=0;j<r.length;j++){
                r2[j] = (r[j]*i)%q;
            }
            if(isSameVektor(r2, l)) return true;
        }     
        return false;
    }
    
    public static void printZHKs(ArrayList<ArrayList<int[]>> ZHKs) {
    	
    	for(int i=0; i < ZHKs.size(); i++) {
    		System.out.println("ZHK "+ i + " :");
    		for(int j=0; j <ZHKs.get(i).size(); j++) {
    			for(int k=0; k < ZHKs.get(i).get(j).length;k++) {
    			System.out.print(ZHKs.get(i).get(j)[k]);
    			}
    			System.out.println("");
    		}
    			
    	}
    	
    }
    
    public static int[][] TransponiereMatrix (int[][] M){
    	int[][] erg = new int[M[0].length][M.length];
    	
    	for(int i=0; i < M.length; i++) {
    		for(int j=0; j < M[i].length; j++) {
    			erg[j][i] = M[i][j];
    		}
    	}	
    	
    	return erg;
    }
    
    public static int[][] getRandomErzeuger (int k, int q){
    	
    	Random r = new Random();
    	int[][] erzeuger = new int [k][k];
    	
    	for(int i = 0; i < k; i++) {
    		for(int j = 0; j < k; j++) {
    			
    			erzeuger[i][j] = r.nextInt()%q;
    			if(erzeuger[i][j] < 0) {
    				erzeuger[i][j] *= -1;
    			}
    		}
    	}
    	
    	
    	return erzeuger;
    }

    public static int[] getVektorC(int[][] AS) {
    	int [] c = new int[AS[0].length];
    	
    	for(int j=0; j < c.length; j++) {
    		for(int i=0; i < AS.length; i++) {
    			if(i==0) {
    				c[j]=AS[i][j];
    			}else if(AS[i][j]>=c[j]) {
    				c[j]=AS[i][j];
    			}
    			
    		}
    	}
    	return c;
    }

    public static int[] getVektorX(ArrayList<GRBVar> xn) {
    	
    	int[] x = new int[xn.size()];
    	
    	for(int i=0; i < x.length; i++) {
    		try {
				x[i] = (int) xn.get(i).get(GRB.DoubleAttr.X);
			} catch (GRBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	return x;
    	
    }
}

