import gurobi.*;
import java.util.ArrayList;
public  class Gurobi {
	
	
	
	
	public void OptimizeProblem(int[][] A, int b) {
		try {
			GRBEnv env = new GRBEnv("CT.log");
			GRBModel model = new GRBModel(env);
			//create x-Vektor
			
			ArrayList<GRBVar> xn = new ArrayList<GRBVar>();
			
			for(int i = 0 ; i < A[0].length;i++) {
				GRBVar xi = model.addVar(0, GRB.INFINITY, 0, GRB.INTEGER, "x"+i);
				
				xn.add(xi);
						
			}
			
			//Objective to Maximize c*x0 + c*x1 + ... + c*xn-1
			
			double c = 1.0;
			
			GRBLinExpr expr = new GRBLinExpr();
			for(int i = 0; i < xn.size(); i++) {
				expr.addTerm(c, xn.get(i));
			}
			model.setObjective(expr, GRB.MAXIMIZE);
			
			
			expr = new GRBLinExpr();
			
			for(int i = 0; i < A.length;i++) {
				for(int j = 0; i < A[i].length;j++) {
					if(A[i][j]!=0) {
						expr.addTerm(A[i][j], xn.get(j));
					}
				}
				model.addConstr(expr, GRB.LESS_EQUAL, b, "c"+i);
			}
			
			model.optimize();	
			
			//Output
			for(int i = 0; i < xn.size(); i++) {
				
			System.out.println(xn.get(i).get(GRB.StringAttr.VarName)
					+ " " +xn.get(i).get(GRB.DoubleAttr.X));
			
			}
			
			System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));
			
			// Dispose of model and environment
			model.dispose();
			env.dispose();
			
		} catch (GRBException e) {
			
			System.out.println("Error code: " + e.getErrorCode() + ". " +
					e.getMessage());
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		double b = 2.0; // 4 ist willk�rlich gew�hlt

		try {
			GRBEnv env = new GRBEnv("CT.log");
			GRBModel model = new GRBModel(env);
			
			/*
			 *			0	1	0	0 		x1			2
			 *			1	0	0	0  		x2			2
			 *     A=					X			<= 
			 * 			0	0	0	1		x3			2			
			 * 			0	0	1	0 		x4			2
			 * 
			 * 
			 * 			a11*x1 + a12*x2 + a13*x3 + a14*x4  <= 2
			 * 			a21*x1 + a22*x2 + a23*x3 + a24*x4  <= 2
			 * 			a31*x1 + a32*x2 + a33*x3 + a34*x4  <= 2
			 * 			a41*x1 + a42*x2 + a43*x3 + a44*x4  <= 2
			 * 
			 * 
			 * 			Optimize
			 * 			1*x1 + 1*x2 + 1*x3 + 1*x4    .....   1*xn
			 * 
			 * 			
			 * 
			 * 
			 */
			
		
			GRBVar x1 = model.addVar(0, GRB.INFINITY, 0, GRB.INTEGER, "x1");
			GRBVar x2 = model.addVar(0, GRB.INFINITY, 0, GRB.INTEGER, "x2");
			GRBVar x3 = model.addVar(0, GRB.INFINITY, 0, GRB.INTEGER, "x3");
			GRBVar x4 = model.addVar(0, GRB.INFINITY, 0, GRB.INTEGER, "x4");
			
			//GRBVar c = model.addVar(0, 1, 0, GRB.INTEGER,"c");
			
			// Skalar {1,x} 1*x0 + 1*x1 + 1*x2
			
			
			
			
			GRBLinExpr expr = new GRBLinExpr();
			expr.addTerm(1.0, x1);expr.addTerm(1.0, x2);expr.addTerm(1.0, x3);expr.addTerm(1.0, x4);
			model.setObjective(expr, GRB.MAXIMIZE);
			
			
			
			
			//Example
			//A <= b
			GRBQuadExpr expr1 = new GRBQuadExpr();
			expr1.addTerm(1.0,x2);
			model.addQConstr(expr1, GRB.LESS_EQUAL, b, "c1");
			
			expr1 = new GRBQuadExpr();
			expr1.addTerm(1.0,x1);
			model.addQConstr(expr1, GRB.LESS_EQUAL, b, "c2");
			
			expr1 = new GRBQuadExpr();
			expr1.addTerm(1.0,x4);
			model.addQConstr(expr1, GRB.LESS_EQUAL, b, "c3");
			
			expr1 = new GRBQuadExpr();
			expr1.addTerm(1.0, x3);
			model.addQConstr(expr1, GRB.LESS_EQUAL, b, "c4");
			
			
			model.optimize();			
			
			System.out.println(x1.get(GRB.StringAttr.VarName)
					+ " " +x1.get(GRB.DoubleAttr.X));
			System.out.println(x2.get(GRB.StringAttr.VarName)
					+ " " +x2.get(GRB.DoubleAttr.X));
			System.out.println(x3.get(GRB.StringAttr.VarName)
					+ " " +x3.get(GRB.DoubleAttr.X));
			System.out.println(x4.get(GRB.StringAttr.VarName)
					+ " " +x4.get(GRB.DoubleAttr.X));
			
			System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));
			
			// Dispose of model and environment
			model.dispose();
			env.dispose();
			
			
		} catch (GRBException e) {
			
			System.out.println("Error code: " + e.getErrorCode() + ". " +
					e.getMessage());
		}
		
	}

}